const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave:false,
  pages: {
      index: {
        // page 的入口
        entry: 'src/main.js',
        // 模板来源
        template: 'public/index.html',
        // 在 dist/index.html 的输出
        filename: 'index.html',
        // 当使用 title 选项时，
        // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
        title: '后台首页',
        // 在这个页面中包含的块，默认情况下会包含
        // 提取出来的通用 chunk 和 vendor chunk。
        chunks: ['chunk-vendors', 'chunk-common', 'index']
      },
      login: {
        entry: 'src/login.js',
        template: 'public/login.html',
        title: '管理员登录',
        chunks: ['chunk-vendors', 'chunk-common', 'login']
      },
      userLogin: {
        entry: 'src/userLogin.js',
        template: 'public/userLogin.html',
        title: '业主登录',
        chunks: ['chunk-vendors', 'chunk-common', 'userLogin']
      },
        userIndex: {
        entry: 'src/userIndex.js',
        template: 'public/userIndex.html',
        title: '业主登录',
        chunks: ['chunk-vendors', 'chunk-common', 'userIndex']
      }    
    }
})
