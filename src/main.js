import Vue from 'vue';
import App from './App.vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router';
// import '@/assets/fep_font/iconfont.css';
import '@/assets/icon/iconfont.css';

// import './assets/css/pintuer.css';
import axios from "axios";
//import VueAxios from "axios";
import echarts from 'echarts'
Vue.prototype.$echarts = echarts

Vue.prototype.$axios = axios;
//Vue.use(VueAxios, axios);
Vue.config.productionTip = false;

axios.defaults.baseURL = 'http://localhost:8080/api/v1';
Vue.use(ElementUI);

axios.interceptors.request.use(function (config) {
    let pathName = location.pathname;
    if (localStorage.getItem('jwt')) {
        if (pathName != '/' && pathName != '/login') {
            config.headers.common['jwt'] = localStorage.getItem('jwt');
        }
    }
    //   else{
    //      ElementUI.Message({
    //             message: '登录过期，请重启登录',
    //             duration: 2000,
    //             type: 'error'
    //         });
    //         setTimeout(()=>{
    //             location.href="/login";
    //         },2000)

    //   }
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
    switch (response.data.code) {
        case 200:
            if(response.headers.jwt){
                localStorage.setItem("jwt",response.headers.jwt);
            }
            break;
        case 400:
            ElementUI.Message({
                message: '失败',
                type: 'error'
            });
            break;
        case 401:
            ElementUI.Message({
                message: '账户或密码错误',
                type: 'warning'
            });
            break;
        case 402:
            ElementUI.Message({
                message: '验证码错误',
                type: 'warning'
            });
            break;
        case 403:
            ElementUI.Message({
                message: '登录过期，请重新登录',
                type: 'warning'

            });
            setTimeout(() => {
                location.href = "/login";
            }, 2000)
            break;
        case 500:
            ElementUI.Message({
                message: '服务器错误',
                type: 'error'
            });
            break;
        case 700:
            ElementUI.Message({
                message: '您权限不足',
                type: 'warning'
            });
            break;
        case 800:
            ElementUI.Message({
                message: '填写参数有误',
                type: 'warning'
            });
            break;
        default:
            break;
    }
    // if (response.data.code == "400") {
    //     ElementUI.Message({
    //         message: '失败',
    //         type: 'error'
    //     });
    // } else if (response.data.code == "401") {
    //     ElementUI.Message({
    //         message: '账户或密码错误',
    //         type: 'warning'
    //     });
    // } else if (response.data.code == "402") {
    //     ElementUI.Message({
    //         message: '验证码错误',
    //         type: 'warning'
    //     });
    // } else if (response.data.code == "403") {
    //     ElementUI.Message({
    //         message: '登录过期，请重新登录',
    //         type: 'warning'

    //     });
    //     setTimeout(() => {
    //         location.href = "/login";
    //     }, 2000)
    // } else if (response.data.code == "500") {
    //     ElementUI.Message({
    //         message: '服务器错误',
    //         type: 'error'
    //     });
    // } else if (response.data.code == "700") {
    //     ElementUI.Message({
    //         message: '您权限不足',
    //         type: 'warning'
    //     });
    // } else if (response.data.code == "800") {
    //     ElementUI.Message({
    //         message: '填写参数有误',
    //         type: 'warning'
    //     });
    // }
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});


new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
