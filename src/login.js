
import Vue from 'vue';
import Login from './LoginApp.vue';

import './assets/css/admin.css';
import './assets/css/pintuer.css';
import axios from 'axios';
Vue.prototype.$axios=axios;

import { Message } from "element-ui";
// Vue.use(Message);
Vue.component(Message.name, Message)
Vue.prototype.$message = Message;

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

Vue.config.productionTip = false;


new Vue({
    axios,
  render: h => h(Login),
}).$mount('#login')
