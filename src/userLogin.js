
import Vue from 'vue';
import UserLogin from './UserLoginApp.vue';
import './assets/css/admin.css';
import './assets/css/pintuer.css';
import axios from 'axios';
Vue.prototype.$axios=axios;

Vue.config.productionTip = false;


new Vue({
    axios,
  render: h => h(UserLogin),
}).$mount('#userLogin')
