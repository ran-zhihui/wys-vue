import Vue from 'vue'
import VueRouter from 'vue-router'
import RouterCar from '../components/car/RouterCar.vue'
import RouterPark from '../components/car/RouterPark.vue'
import RouterCarInOut from '../components/car/RouterCarInOut.vue'
import RepairsPlace from '../components/repairs/RepairsPlace.vue'
import RepairsRecord from '../components/repairs/RepairsRecord.vue'
import RepairsType from '../components/repairs/RepairsType.vue'
import ChargeConfig from '../components/config/ChargeConfig.vue'
import ChargeAccount from '../components/config/ChargeAccount.vue'
import PropertyStaff from '../components/config/PropertyStaff.vue'
import Landlord from '../components/landlord/Landlord.vue'
import PayMentRecord from '../components/payments/PayMentRecords.vue'
import PaySta from '../components/payments/PayMentSta.vue'
import RoutRoom from '../components/room/Room.vue'
import Index1 from '../components/index/Index1.vue'
import RoutRoomView from '../components/room/RoomView.vue'
import Notice from '../components/notice/Notice.vue'
import Password from '../components/index/Password.vue'
import PermManager from '../components/perm/PermManager.vue'
import RoleManager from '../components/perm/RoleManager.vue'
Vue.use(VueRouter)

const routes = [

  {
    path: '/car/car',
    name: 'RouterCar',
    component: RouterCar
  },
  {
    path: '/car/park',
    name: 'RouterPark',
    component: RouterPark
  }, {
    path: '/car/CarInOut',
    name: 'RouterCarInOut',
    component: RouterCarInOut
  }, {
    path: '/charge/config',
    name: 'ChargeConfig',
    component: ChargeConfig
  }, {
    path: '/charge/account',
    name: 'ChargeAccount',
    component: ChargeAccount
  }, {
    path: '/property/staff',
    name: 'PropertyStaff',
    component: PropertyStaff
  }, {
    path: '/yz/list',
    name: 'Landlord',
    component: Landlord
  },
  {
    path: '/bx/place',
    name: 'RepairsPlace',
    component: RepairsPlace
  },
  {
    path: '/bx/record',
    name: 'RepairsRecord',
    component: RepairsRecord
  },
  {
    path: '/bx/type',
    name: 'RepairsType',
    component: RepairsType
  },
  {
    path: '/pay/paySta',
    name: 'PaySta',
    component: PaySta
  },
  {
    path: '/pay/record',
    name: 'PayMentRecord',
    component: PayMentRecord
  },
  {
    path: '/room/list',
    name: 'RoutRoom',
    component: RoutRoom
  },
  {
    path: '/index/Index1',
    name: 'Index1',
    component: Index1
  }
  ,
{
    path: '/room/view',
    name: 'RoutRoomView',
    component: RoutRoomView

},
{
  path: '/notice/list',
  name: 'Notice',
  component: Notice
},
{
    path: '/index/Password',
    name: 'Password',
    component: Password
},
{   path: '*',
    redirect: '/index/Index1'
},
{
  path: '/perm/list',
    name: 'PermManager',
    component: PermManager
},
{
  path: '/role/list',
    name: 'RoleManager',
    component: RoleManager
}
]

const router = new VueRouter({
  routes
})

export default router
